/**
 * Created by Jakk on 9/11/2016 AD.
 */
import {AUTH_USER, UNAUTH_USER, AUTH_ERROR} from '../actions/actionTypes';

export default function (state = {}, action) {
    switch (action.type) {
        case AUTH_USER:
            return {...state, authenticated: true};
        case UNAUTH_USER:
            return {...state, authenticated: false};
        case AUTH_ERROR:
            return {...state, authError: action.authError};

    }
    return state;
}
