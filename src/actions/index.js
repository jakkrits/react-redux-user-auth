import axios from 'axios';
import {browserHistory} from 'react-router';
import {AUTH_USER, AUTH_ERROR} from '../actions/actionTypes';

const API_URL = 'http://localhost:3090';

export function signinUser({email, password}) {
    return function (dispatch) {
        //Submit email/password to server
        console.log('SIGNING IN USER');

        //{email, password} === {email: email, password: password}
        axios.post(`${API_URL}/signin`, {email, password})
            .then(response => {
                //if request is good..
                // - update state to indicate user is authenticated
                dispatch({type: AUTH_USER});

                // - save jwt token
                localStorage.setItem('token', response.data.token);

                // - redirect to route '/feature'
                browserHistory.push('/feature');
            })
            .catch(() => {
                //if request is bad...
                // - show error to user
                dispatch(authError('Bad Login Info'));
            });
    }
}


export function authError(authError) {
    return {
        type: AUTH_ERROR,
        authError
    };
}